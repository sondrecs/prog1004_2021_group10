#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
using namespace std;


class Task{
    private:
        int priority;
        int year;
        int month;
        int day;
        int hours;
        int minutes;
        string description;
        string category;
        int startyear;
        int startmonth;
        int startday;
        bool finished;
    public:
        Task();
        Task(ifstream & in);
        void writeFile(ofstream & out);
        void writeData();
        void writeTask();
        void finishTask();
        void changeData();
        int getPriority();
        int getYear();
        int getMonth();
        int getDay();
        int getHours();
        int getMinutes();
        bool getStatus();
};


void menuWrite();
void readFile();
void writeFile();
void writeList();
void viewTask(int);
void writeCompleted();
void newTask();
void deleteTask(int);
void orderTasksPriority();
void orderTasksDeadline();
void wait(string);
char getCharInput();
bool isNumber(string);
int getIntInput(string);
string getStringInput();

const char* const TASKFILE = "SAVEDTASKS.DTA"; //The file used to save tasks
int savedTasks = 0; //How many tasks there are currently stored in the map
bool timeOrder = false; //If the program should order by deadline
vector <int> taskOrder; //Vector that stores the tasks in wanted order
map <int, Task*> gTask; //Map that stores all task info

//Default constructor for Task
Task::Task() {
    priority = getIntInput("Priority: ");
    year = getIntInput("Deadline year:");
    do{
        month = getIntInput("Deadline month(1 - 12): ");
    } while (month < 1 || month > 12);
    do {
        day = getIntInput("Deadline day (1 - 31): ");
    } while (day < 1 || day > 31);
    do {
        hours = getIntInput("Hour of day (0 - 23):");
    } while (hours < 0 || hours > 23);
    do {
        minutes = getIntInput("Minutes (0 - 59): ");
    } while (minutes < 0 || minutes > 59);
    cout << "Description:";
    getline(cin,description);
    cout << "Category:";
    getline(cin,category);
    do{
        startyear = getIntInput("Start year:");
        if (startyear > year){
            cout << "Start date cannot be after deadline\n";
        }
    } while (startyear > year);
    do{
        startmonth = getIntInput("Start month(1 - 12):");
        if (startyear == year && startmonth > month){
            cout << "Start date cannot be after deadline\n";
        }
    } while (startmonth < 1 || startmonth > 12 || (startyear == year && startmonth > month));
    do {
        startday = getIntInput("Start day (1 - 31):");
        if(startyear == year && startmonth == month && startday > day){
            cout << "Start date cannot be after deadline\n";
        }
    } while (startday < 1 || startday > 31 || (startyear == year && startmonth == month && startday > day));
    finished = false;
}


//Reads task information form a file
Task::Task(ifstream &in) {
    in >> priority;
    in.ignore();
    in >> day;
    in >> month;
    in >> year;
    in.ignore();
    in >> hours;
    in >> minutes;
    in.ignore();
    getline(in,description);
    getline(in,category);
    in >> startday;
    in >> startmonth;
    in >> startyear;
    in.ignore();
    in >> finished;
    in.ignore();
    in.ignore();
}

//Writes task information to a file
void Task::writeFile(ofstream & out) {
    out << "\n" << priority << "\n"
        << day << " " << month << " " << year << "\n"
        << hours << " " << minutes << "\n"
        << description << "\n"
        << category << "\n"
        << startday << " " << startmonth << " "<< startyear << "\n"
        << finished << "\n";

}


//Writes list of tasks
void Task::writeData() {
    cout << priority << "\t   " << day<<'/'<<month<<'/'<<year << '\t';
        if(hours < 10){
            cout << " ";
        }
        cout << hours << ":";
        if(minutes < 10){
            cout << "0";
        }
        cout << minutes << "\t" << description << '\n';
}

//Writes information about one task
void Task::writeTask() {
    cout << "\n\n*****************\n";
    cout << "Priority: \t" << priority << '\n';
    cout << "Deadline: \t" << day<<'/'<<month<<'/'<<year << '\n';
    cout << "Time: \t\t" << hours << ":";
    if(minutes < 10){
        cout << "0";
    }
    cout << minutes << '\n';
    cout << "Description: \t" << description  << '\n';
    cout << "Category: \t" << category << '\n';
    if (finished){
        cout << "Status: \tFinished" << '\n';
    } else{
        cout << "Status: \tIn progress" << '\n';
    }
    cout << "Start date: \t" << startday<<'/'<<startmonth<<'/'<<startyear << '\n';
    cout << "*****************\n";
}

//sets a task to finished
void Task::finishTask() {
    if(finished){
        finished = false;
    }else{
        finished = true;
    }
}

//returns the status of a task
bool Task::getStatus() {
    return finished;
}

//returns priority of a task
int Task::getPriority() {
    return priority;
}

//returns deadline year of a task
int Task::getYear() {
    return year;
}

//returns deadline month of a task
int Task::getMonth() {
    return month;
}

//returns deadline day of a task
int Task::getDay() {
    return day;
}

//returns deadline hour of a task
int Task::getHours() {
    return hours;
}

//returns deadline minute of a task
int Task::getMinutes() {
    return minutes;
}

//changes chosen data in a task
void Task::changeData() {
    char command = ' ';
    while (command != 'B'){
        writeTask();
        cout << "What would you like to change?\n";
        cout << "(1)Priority\n";
        cout << "(2)Deadline\n";
        cout << "(3)Time\n";
        cout << "(4)Description\n";
        cout << "(5)Category\n";
        cout << "(6)Start date\n";
        cout << "*****************\n";
        cout << "Press number (..) to change.\n";
        cout << "Press (B) to go back\n\n";
        cout << "*****************\n";
        command = getCharInput();
        switch (command) {
            case '1':
                priority = getIntInput("Priority: ");
                break;
            case '2':
                year = getIntInput("Deadline year:");
                do{
                    month = getIntInput("Deadline month(1 - 12): ");
                } while (month < 1 || month > 12);
                do {
                    day = getIntInput("Deadline day (1 - 31): ");
                } while (day < 1 || day > 31);
                break;
            case '3':
                do {
                    hours = getIntInput("Hour of day (0 - 23):");
                } while (hours < 0 || hours > 23);
                do {
                    minutes = getIntInput("Minutes (0 - 59): ");
                } while (minutes < 0 || minutes > 59);
                break;
            case '4':
                cout << "Description:";
                getline(cin,description);
                break;
            case '5':
                cout << "Category:";
                getline(cin,category);
                break;
            case '6':
                do{
                    startyear = getIntInput("Start year:");
                    if (startyear > year){
                        cout << "Start date cannot be after deadline\n";
                    }
                } while (startyear > year);
                do{
                    startmonth = getIntInput("Start month(1 - 12):");
                    if (startyear == year && startmonth > month){
                        cout << "Start date cannot be after deadline\n";
                    }
                } while (startmonth < 1 || startmonth > 12 || (startyear == year && startmonth > month));
                do {
                    startday = getIntInput("Start day (1 - 31):");
                    if(startyear == year && startmonth == month && startday > day){
                        cout << "Start date cannot be after deadline\n";
                    }
                } while (startday < 1 || startday > 31 || (startyear == year && startmonth == month && startday > day));
                break;
            case 'B':
                cout << "B";
                break;
            default:
                cout<<"Invalid command\n";
                break;
        }
    }
}

//The main menu and code
int main() {
    readFile();
    char command = ' ';
    while (command != 'Q'){
        menuWrite();
        command = getCharInput();
        switch (command) {
            case 'T':   writeList();                break;
            case 'F':   writeCompleted();           break;
            case 'C':   newTask();                  break;
            case 'Q':   break;
            default:    cout<<"Invalid command\n";  break;
        }
    }
    writeFile();
}

//Prints menu options
void menuWrite(){
    cout << "\n\n*****************\n";
    cout << "Menu choices\n";
    cout << "Press letter in (..) to execute command\n\n";
    cout << "(T)o do list\n";
    cout << "(F)inished tasks\n";
    cout << "(C)reate task\n";
    cout << "(Q)uit\n";
    cout << "*****************\n";
}

//Reads tasks from file and stores them
void readFile(){
    ifstream in(TASKFILE);
    if(in){
        cout << "File: " << TASKFILE << " has been opened";
        in >> savedTasks;
        in.ignore();
        for(int i = 1; i <= savedTasks; i++){
            gTask[i] = new Task(in);
        }
        in.close();
    } else{
        cout << "Couldnt open file: " << TASKFILE;
    }


}

//Writes tasks to file so that they can be accessed later
void writeFile(){
    ofstream out("SAVEDTASKS.DTA");
    if(out){
        orderTasksPriority();
        out << taskOrder.size();
        for(int i = 0; i < taskOrder.size(); i++){
            gTask[taskOrder[i]]->writeFile(out);
        }
    }
    out.close();

}

//prints list of tasks and lets you choose one
void writeList(){
    string command = "B";
    int num;
    bool input;
    do {
        if(timeOrder){
            orderTasksDeadline();
        }else{
            orderTasksPriority();
        }
        input = false;
        cout << "\n\n*****************\n";
        cout << "Index   Priority   Date         Time    Description\n";
        if (taskOrder.size() > 0){
            for (int i = 0; i < taskOrder.size(); i++){
                cout << i+1 << '\t';
                gTask[taskOrder[i]]->writeData();
            }
        }
        cout << "\n*****************\n";
        cout << "Press the index number to view task.\n";
        if (timeOrder){
            cout << "Press (O) to order by priority\n";
        }else{
            cout << "Press (O) to order by deadline\n";
        }
        cout << "Press (B) to go back\n";
        cout << "*****************\n";

        do {
            command = getStringInput();
            if (command != "B" && command != "O") {
                if (isNumber(command)) {
                    num = stoi(command) - 1;
                    if (num >= 0 && num < savedTasks) {
                        input = true;
                        viewTask(taskOrder[num]);
                    } else {
                        cout << "invalid number\n";
                    }
                } else {
                    cout << "invalid command\n";
                }
            } else {
                input = true;
            }
        } while (!input);
        if(command == "O") {
            if (timeOrder) {
                timeOrder = false;
            } else {
                timeOrder = true;
            }
        }
    } while (command != "B");

}

//prints more information of a task and options
void viewTask(int i){
    char command = ' ';
    do{
    gTask[i]->writeTask();
    cout << "MENU CHOICES\n";
    cout << "(E)dit\n";
    cout << "(F)inished\n";
    cout << "(D)elete task\n";
    cout << "(B)ack\n";
        command = getCharInput();
        switch (command) {
            case 'E': gTask[i]->changeData(); command = 'B'; break;
            case 'F': gTask[i]->finishTask(); break;
            case 'D': deleteTask(i); command = 'B'; break;
            case 'B': break;
            default:  cout<<"Invalid command\n";  break;
        }
    }while (command != 'B');
}

//Prints all completed tasks in order of priority (Tafheem)
void writeCompleted(){

    orderTasksPriority();
    cout << "\n\n*****************\n";
    cout << "Index   Priority   Date         Time   Description\n";
    if (taskOrder.size() > 0){
        for (int i = 0; i < taskOrder.size(); i++){
            if(gTask[taskOrder[i]]->getStatus()){
                cout << i+1 << '\t';
                gTask[taskOrder[i]]->writeData();
            }
        }
    }
}

//Adds a new task and gives it a valid priority
void newTask(){
    char command = ' ';
    cout << "\n\n*****************\n";
    savedTasks++;
    gTask[savedTasks] = new Task;
    cout << "\n*****************\n";
    cout << "Are all inputs correct?\n";
    cout << "(Y)es / (N)o\n";
    do{
        command = getCharInput();
    } while (command != 'Y' && command != 'N');
    if (command == 'N'){
        gTask[savedTasks]->changeData();
    }
    wait("The task has been saved!");

}


//Deletes task and fills the gaps
void deleteTask(int i){
    char command = ' ';
    cout << "\n*****************\n";
    cout << "Are you sure you want to delete task "<< i <<"?\n";
    cout << "(Y)es / (N)o\n";
    do{
        command = getCharInput();
    } while (command != 'Y' && command != 'N');
    if (command == 'Y'){
        gTask.erase(i);
        cout << "Task deleted\n";
        savedTasks--;
    }else{
        cout << "Task has not been deleted \n";
    }
}



//Orders the indexes of the tasks by priority
void orderTasksPriority(){
    bool done;
    taskOrder.clear();
    for(auto & i: gTask){
        done = false;
        for(int j = 0; j < taskOrder.size() && !done; j++){
            if(i.second->getPriority() > gTask[taskOrder[j]]->getPriority()){
                taskOrder.insert(taskOrder.begin() + j, i.first);
                done = true;
            }
        }
        if(!done){
            taskOrder.push_back(i.first);
        }
    }
}

//Orders the indexes of the tasks by their deadline and lastly by priority
void orderTasksDeadline(){
    bool done;
    taskOrder.clear();
    for(auto & i: gTask){
        done = false;
        for(int j = 0; j < taskOrder.size() && !done; j++){
            if(i.second->getYear() < gTask[taskOrder[j]]->getYear()){
                taskOrder.insert(taskOrder.begin() + j, i.first);
                done = true;
            }else if(i.second->getYear() == gTask[taskOrder[j]]->getYear()){

                if(i.second->getMonth() < gTask[taskOrder[j]]->getMonth()){
                    taskOrder.insert(taskOrder.begin() + j, i.first);
                    done = true;
                }else if(i.second->getMonth() == gTask[taskOrder[j]]->getMonth()){

                    if(i.second->getDay() < gTask[taskOrder[j]]->getDay()){
                        taskOrder.insert(taskOrder.begin() + j, i.first);
                        done = true;
                    }else if(i.second->getDay() == gTask[taskOrder[j]]->getDay()){

                        if(i.second->getHours() < gTask[taskOrder[j]]->getHours()){
                            taskOrder.insert(taskOrder.begin() + j, i.first);
                            done = true;
                        }else if(i.second->getHours() == gTask[taskOrder[j]]->getHours()){

                            if(i.second->getMinutes() < gTask[taskOrder[j]]->getMinutes()){
                                taskOrder.insert(taskOrder.begin() + j, i.first);
                                done = true;
                            }else if(i.second->getMinutes() == gTask[taskOrder[j]]->getMinutes()){

                                if(i.second->getPriority() > gTask[taskOrder[j]]->getPriority()){
                                    taskOrder.insert(taskOrder.begin() + j, i.first);
                                    done = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!done){
            taskOrder.push_back(i.first);
        }
    }
}

//Waits for the user to press ENTER
void wait(string s){
    string buffer;
    cout << "\n\n*****************\n";
    cout << s << '\n';
    cout << "Press ENTER to go back to main menu\n";
    cout << "*****************\n";
    getline(cin,buffer);
}

//Waits for the user to write a character and returns it
char getCharInput(){
    char input;
    //cout << "input:";
    cin >> input;
    cin.ignore(1,'\n');
    return toupper(input);
}

//Checks if a string only consists of numbers and returns true or false
bool isNumber(string s){
    bool number = true;
    for(int i = 0; i < s.length(); i++){
        if(!isdigit(s[i])){
            number = false;
        }
    }
    return number;
}


int getIntInput(string s){
    int out;
    string temp;

    while (true){
        cout << s;
        getline(cin, temp);

        if (isNumber(temp)){
            out = stoi(temp);
            return out;
        }else{
            cout << "Not a valid value\n";
        }
    }
}

string getStringInput(){
    string input;
    cin >> input;
    cin.ignore(1,'\n');
    input[0] = toupper(input[0]);
    return input;
}
